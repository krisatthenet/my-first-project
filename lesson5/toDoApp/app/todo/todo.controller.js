(function(){
  var app = angular.module("ToDoApp");
  
  app.controller("ToDoListController", function($localStorage) {
    var vm = this;
    
    $localStorage.itemsList = $localStorage.itemsList || [];
    
    vm.todosList = $localStorage.itemsList;
    vm.addItem = addItem;
    vm.deleteItem = deletedItem;
    
    function addItem() {
      if(!vm.inputValue) {
        return;
      }
      vm.todosList.push({
        name: vm.inputValue,
        isDone: false
      });
      vm.inputValue = "";
    }
    
    function deletedItem(item) {
      var index = vm.todosList.indexOf(item);
      vm.todosList.splice(index, 1);
    }
  });
})();