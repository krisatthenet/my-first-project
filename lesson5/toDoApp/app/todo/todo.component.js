(function(){
  var app = angular.module("ToDoApp");
  
  app.component("todoList", {
    templateUrl: "todo/todo.view.html",
    controller: "ToDoListController",
    controllerAs: "vm"
  });
})();