var weatherModule = (function() {
    
    function init() {
      navigator.geolocation.getCurrentPosition(weatherRequest);
      
    }

      function handleBars(temp, sum, icon) {
        var source = $("#weather-template").html();
        var context = { 
          weatherTemp:temp,
          weatherSum:sum,
          weatherIcon:icon
                  };
        var template = Handlebars.compile(source);
        var htmlInput = template(context);
        $("#weather").append(htmlInput);
    
  }
      
      function weatherRequest(position) {
        var apiKey = "f6ae2dd74903e9d81cfb6800790bebd6";
        var lat = position.coords.latitude;
        var long = position.coords.longitude;
        var url = "https://api.forecast.io/forecast/";    

        $.getJSON(url + apiKey + "/" + lat + "," + long + "?units=si&callback=?", function(data) {
            var summary = data.currently.summary;
            var temperature = data.currently.temperature;
            handleBars(temperature, summary);
            if (temperature < 5) {
              $("#weather_temp").addClass("cold");
            }
            if (summary.includes("Cloudy")) {
              $("#icon").addClass("glyphicon glyphicon-cloud");
            }
            else if (summary.includes("Clear")) {
              $("#icon").addClass("glyphicon glyphicon-hd-video");
            }
            var img = new Image();
            img.src = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + long + "&zoom=13&size=300x300&sensor=false";
            $("#weather").append(img);
        });
      }
    
  
  return {
    init: init
  };
})();

weatherModule.init();