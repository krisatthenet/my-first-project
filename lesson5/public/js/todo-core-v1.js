var toDoModule = (function() { 
  // is local storage cia turi paimt
  var userInputs = [];
  var source = $("#entry-template").html();
  var context = {title: "ToDo app", 
                 body: "What do you need to do?"};
  var template = Handlebars.compile(source);
  var html = template(context);

  $(document.body).append(html);
  
  function init() {
    $(".body").submit(function(event) {
      event.preventDefault();
      addInputs();
    });

    $(".taskList").on("click", ".remove", onClickRemove);

    $(".taskList").on("click", ".completed", onClickComplete);
  }
  
  function addInputs() { 
    var getToDoInput = $('#toDoInput').val();
    var total = userInputs.push(getToDoInput);
    var source = $("#toDoList-template").html();
    var context = {userInput:getToDoInput};
    var template = Handlebars.compile(source);
    var htmlInput = template(context);


    $(".taskList").append(htmlInput);
    console.log(total + " Is: " + userInputs);     
    return false;
  }
  
  function onClickRemove(event) {
    $(event.target).parent().remove();
  }
  
  function onClickComplete(event) {
    $(event.target).parent().addClass("bg-success");
  }
  
  // Return an object exposed to the public
  return {
    init: init
  };
})();

toDoModule.init();