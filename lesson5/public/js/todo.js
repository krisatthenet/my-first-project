var toDoModule = (function() { 
  var userInputs = [];
  var source = $("#entry-template").html();
  var context = {
    title: "ToDo app", 
    body: "What do you need to do?"
  };
  var template = Handlebars.compile(source);
  var html = template(context);

  $('body').append(html);
  
  function init() {
    
    $(".body").submit(function(event) {
      event.preventDefault();
      addInputs();
    });
    
    if(localStorage.getItem('todos')) { 
      inputsFromStorage();    
    }

    $(".taskList").on("click", ".remove", onClickRemove);

    $(".taskList").on("click", ".completed", onClickComplete);
    
    $("#clear").click(onClickRefresh);
    
  }
  
  function handleBars(variable) {
    var source = $("#toDoList-template").html();
    var context = { 
      userInput:variable 
    };
    template = Handlebars.compile(source);
    var htmlInput = template(context);
    $(".taskList").append(htmlInput);
    
  }
  
  function addInputs() { 
    var getToDoInput = $('#toDoInput').val();
    handleBars(getToDoInput);
    userInputs.push(getToDoInput);
    localStorage.setItem('todos', JSON.stringify(userInputs));
    return false;
  }
  
  function inputsFromStorage() {
    var fromStorage = JSON.parse(localStorage.getItem('todos'));
    for (var i = 0; i < fromStorage.length; i++) {
      handleBars(fromStorage[i]);
    }
  }    
  
  function onClickRemove(event) {
    $(event.target).parents("li").remove();
  }
  
  function onClickComplete(event) {
    $(event.target).parents("li").toggleClass("bg-success");
  }
  
  function onClickRefresh() {
    window.localStorage.clear();
    location.reload();
    return false;
  }
  
  return {
    init: init
  };
})();

toDoModule.init();