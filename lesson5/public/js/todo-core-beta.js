var toDoModule = (function() { 
  var userInputs = [];
  var source = $("#entry-template").html();
  var context = {title: "ToDo app", 
                 body: "What do you need to do?"};
  var template = Handlebars.compile(source);
  var html = template(context);

  $(document.body).append(html);
  
  // Return an object exposed to the public
  return {
    // Add inputs to our array
    addInputs: $(".body").submit(function(event) {
      event.preventDefault();
      var getToDoInput = $('#toDoInput').val();
//      var source = $("#toDoList-template").html();
//      var context = {userInput:getToDoInput};
//      var template = Handlebars.compile(source);
//      var htmlInput = template(context);
      // Pradedam append
      var total = userInputs.push(getToDoInput);
      console.log(total + " Is: " + userInputs);
//      $(".taskList").append(htmlInput);
      return false;
    }),
    onClickRemove: $(".taskList").on("click", ".remove", function() {
      $(this).parent().remove();
      return false;
    }),
    onClickComplete: $(".taskList").on("click", ".completed", function() {
      $(this).parent().addClass("bg-success");
      return false;
    })
  };
})();