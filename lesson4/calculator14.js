(function () {
  
  function calculator(x) {
    this.x = x;
  }
calculator.prototype = {
  add : function(x) {
        this.x += x;
        return this;
    },
  subtract : function (x) {
        this.x -= x;
        return this;
    },
  multiply : function(x) {
      this.x *= x;
      return this;
    },
    divide : function(x) {
      this.x /= x;
      return this;
    },
  
   valueOf : function () {
       return this.x;
   }
  
  };
  var calc = new calculator(0);
  var amount = calc.add(5).multiply(2).add(20).divide(3);
  console.log(amount + 10);
})();


  

// REFERENCE:
//(function () {
//  
//  function calculator(x) {
//    this.x = x;
//  }
//    calculator.prototype.add = function (x) {
//      this.x += x;
//      return this;
//    };
//    calculator.prototype.multiply = function (x) {
//      this.x *= x;
//      return this;
//    };
//    calculator.prototype.divide = function (x) {
//      this.x /= x;
//      return this;
//    };
//    calculator.prototype.subtract = function (x) {
//      this.x -= x;
//      return this;
//    };
//    calculator.prototype.valueOf = function () {
//       return this.x;
//   };
//
//  var calc = new calculator(0);
//  var amount = calc.add(5).multiply(2).add(20).divide(3);
//  console.log(amount);
//})();