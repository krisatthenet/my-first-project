function firstFunction (x) {
  return function secondFunction(y) {
    return x+y;
  };
}

console.log(firstFunction(10)(5));