function fizzBuzz(index, end) {
  var array = [];
  var value = 0;
  for (var i = index; i < end; i++) {
    if (i % 15 === 0) {
      value = "FizzBuzz";
    }
    else if (i % 5 === 0) {
      value = "Buzz";
    }
    else if (i % 3 === 0) {
      value = "Fizz";
    }
    else {
      value = i;
    }
    array.push(value);
  }
    return array;
}
console.log(fizzBuzz(1, 100));


// Reference
//function fizzBuzz(start, end){
//  return range(start,end)
//    .map(createFizzBuzzItem);
//}
//
//function createFizzBuzzItem(number) {
//  if (number % 15 ===0) {return "fizzbuzz"}
//  if (number % 5 ===0) {return "buzz"}
//  if (number % 3 ===0) {return "fizz"}
//  return number;
//}