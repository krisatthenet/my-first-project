function sevenAte9 (x) {
  
  if ( typeof x != "string") {
     console.log ("Input is not a string");
    } else {
  
  for (var i = 0; x.includes("797") === true; i++) {
    x = x.replace(/797/g, "77"); // g - global match
    
  }
  return x;
}
}

console.log(sevenAte9("79712312"));
console.log(sevenAte9("79797"));
console.log(sevenAte9(true));

// tarp 7 turi dingt 9