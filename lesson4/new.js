//(function() {
//  for (var i = 0; i < 4; i++) {
//    
//    setTimeout(function() {
//      console.log(i);
//    }, 0);
//  }
//})();


(function() {
  for (var i = 0; i < 4; i++) {

    (function(index) {
      setTimeout(function() {
        console.log(index); 
        },1000);
    })(i);
    
  }

})();

//The function argument to setTimeout is closing over the loop variable. The loop finishes before the first timeout

//Because JavaScript variables only have function scope, the solution is to pass the loop variable to a function that sets the timeout