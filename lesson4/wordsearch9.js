function wordSearch(w) {
  var match = w + "\\b";
  var stringForSearching = "This is a string containing word tomato";
  
  if (stringForSearching.search(match) != -1) {
    console.log("String contains " + w);
      }
  else {
    console.log("String doesn't contain " + w);
  }
}

wordSearch("this");


// search() returns the index of the first match or if not found -1
//full word - \\b