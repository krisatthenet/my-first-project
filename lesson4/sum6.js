function sumArrayNumbers(x) {
  var sum = 0;
  for ( var i = 0; i < x.length; i++) {
    {
      sum = sum + x[i];
    }
  }
  return sum;
}
console.log(sumArrayNumbers([10,6,2,3]));

// OLD VERSION:
//function createTheArray() {
//   return Array.from(arguments);
//}
//
//var sum = createTheArray(1,2,3,4).reduce(add, 0);
//
//function add(a, b) {
//    return a + b;
//}
//
//console.log(sum);