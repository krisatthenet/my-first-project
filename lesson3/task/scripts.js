https://api.forecast.io/forecast/f6ae2dd74903e9d81cfb6800790bebd6/51.5034070,-0.1275920

var ForecastIo = require('forecastio');

var forecastIo = new ForecastIo('f6ae2dd74903e9d81cfb6800790bebd6');
forecastIo.forecast('51.5034070', '-0.1275920').then(function(data) {
  console.log(JSON.stringify(data, null, 2));
});